# DEMO SERVER Casino

## Criterios tomados:

## 1- Opción tomada en la gestión de las transacciones:
He aplicado el caso de que en si realizamos una apuesta, ésta queda registrada en una transacción y si obtenemos un premio, otra transacción con la totalidad del premio.
Sería el caso en de un slots, ya que pago la jugada y si tengo premio recibo un premio, en vez de la opción que sería un poker, ya que si en la partida de
poker gano, también recibo mi apuesta, que no es el caso que he tomado yo.

El servidor usa una BDD sql en memoria que se llama "hsqldb", esta en las dependencias y se configura mediante el fichero schema.sql y data.sql dentro de scripts/hsqldb.
Aquí es dónde guardo los datos que voy creando, a parte de los logs que va sacando el test mientras realiza operaciones.

para comprobar la funcionalidad del servidor, hay test, dentro de Service, llamada CasinoEngineServiceTest, al ejecutarlo simulara la ejecución de dos partidas simultaneas, 
con dos jugadores diferente en dos juegos diferentes y con apuestas aleatorias, para ver que pasaria si intentaran hacer una apuesta mal a media partida.

Está el ApuestaLimiteTest que prueba si intentas apuestas incorrectas para el juego y TiempoLimiteTest que prueba que pasaría si intentara hacer apuesta un jugador que se
le ha agotado el tiempo de juego

## Pasos para probar el funcionamiento del servidor de casino:

Para probar el controlador api (en un principio quería que se pudieran ejecutar los servicios via api y simular la creacion de juegos, jugadores etc. como si fuera un front atacando
mediante urls y usar la documentación de swagger, que esta en las depencias pero no lo he llegado a usar, por la fata de tiempo). He realizado un controlador, el jugadorController,
que sólo tiene dos metodos, devuelve el listado de los jugadores (GET) y creaar un Jugador (POST), al no tener parte front he lanzado las URL's desde la aplicación postman.

Pasos: Run de CasinoDemoApplication.
### GET del controlador: Cuando ha arrancado, vamos al postman y con la opcion GET ponemos la URL http://localhost:8080/api/jugador -> SEND. nos devuelve el JSON con todos los jugadores
### POST del controlador: Cuando ha arrancado, vamos al postman y con la opcion POST url: http://localhost:8080/api/jugador?balance=90&tiempoJuego=10&activo=1&nombre=test&id_proveedor=1 -> SEND. nos devuelve el JSON del jugador creado

Para probar la partida los test, ejecutar los test mencionados anteriormente(CasinoEngineServiceTest, ApuestaLimiteTest,TiempoLimiteTest ), se guarda todo en la BDD y laca los logs de las accione.
Tambien hay test unitarios de los servicios.

## Integración continua
Tengo el proyecto en un sonar de un amigo, para poder analizar mis proyectos, aquí:
[server Casino](http://sonar.jaumemoron.cat/dashboard?id=com.serverCasino%3ACasinoDemo).
