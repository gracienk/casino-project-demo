package com.servercasino.casinodemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CasinoDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(CasinoDemoApplication.class, args);
	}

}
