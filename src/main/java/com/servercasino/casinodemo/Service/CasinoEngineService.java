package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Transaccion;

import java.math.BigDecimal;

public interface CasinoEngineService {
    Jugada realizarJugada(Jugador jugador, Juego juego, BigDecimal apuesta);
}
