package com.servercasino.casinodemo.Service.Impl;

import com.servercasino.casinodemo.Service.CasinoEngineService;
import com.servercasino.casinodemo.Service.JugadaService;
import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Transaccion;
import com.servercasino.casinodemo.repository.JugadaRepository;
import com.servercasino.casinodemo.repository.JugadorRepository;
import com.servercasino.casinodemo.repository.TransaccionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.security.SecureRandom;
import java.time.LocalDateTime;

import static com.servercasino.casinodemo.constants.ErrorMessages.ARGUMENT_EXCEPTION_NULL;

@Service
public class CasinoEngineServiceImpl implements CasinoEngineService {

    @Autowired
    JugadaRepository jugadaRepository;
    @Autowired
    TransaccionRepository transaccionRepository;
    @Autowired
    JugadorRepository jugadorRepository;

    private static final Logger LOGGER = LoggerFactory.getLogger(JugadaService.class);

    @Override
    public Jugada realizarJugada(Jugador jugador, Juego juego, BigDecimal apuesta) {

        if (jugador == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);
        if (juego == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);

        jugador = this.evaluateCanPlay(jugador);

        if(jugador.isActivo() && this.evaluateApuestaIsOk(juego, apuesta)){

            boolean isWin = evaluateProbabilidad(juego.getProbabilidad());
            LOGGER.info("CasinoEngineService -> realizarJugada() -> jugada realizada por el jugador[{}], en el juego [{}] ha resultado: [{}]",jugador.getNombre(), juego.getNombre(), isWin?"Winner":"Looser");

            Jugada jugada = new Jugada();
            jugada.setJugador(jugador);
            jugada.setJuego(juego);
            jugada.setApuesta(apuesta);
            jugada.setWin(isWin);

            jugada = jugadaRepository.save(jugada);

            crearTransaccion(jugada, jugada.getApuesta(), false);
            if(jugada.isWin()){
                crearTransaccion(jugada, juego.getPremio(), true);
            }
            return jugada;
        }
        return null;
    }

    private Transaccion crearTransaccion(Jugada jugada, BigDecimal importe, boolean isWin){
        Jugador jugador = jugada.getJugador();
        Transaccion transaccion = new Transaccion();
        transaccion.setJugada(jugada);
        transaccion.setImporte(importe);
        transaccion.setCreation_date(LocalDateTime.now());

        jugador = updateBalance(jugador, importe, isWin);


        return transaccionRepository.save(transaccion);
    }

    private boolean evaluateProbabilidad(float probabilidad){
        SecureRandom rand = new SecureRandom();
        int upperbound = 100;
        float floatRandom = rand.nextFloat() * upperbound;

        LOGGER.info("CasinoEngineService -> evaluateProbabilidad() -> Probabilidad [{}] -> float Random [{}]",probabilidad, floatRandom);
        if(floatRandom <= probabilidad) {
            return true;
        }
        return false;
    }

    private Jugador evaluateCanPlay(Jugador jugador){

        //comprobamos el balance
        if(jugador.getBalance().compareTo(BigDecimal.ZERO)<=0) {
            jugador.setActivo(false);
            LOGGER.warn("CasinoEngineService -> evaluateCanPlay() -> Jugador [{}] no puede jugar. Balance: [{}]", jugador.getNombre(), jugador.getBalance());
        }
        //comprobamos la actividad
        if(!jugador.isActivo()) {
            LOGGER.warn("CasinoEngineService -> evaluateCanPlay() -> Jugador [{}] es inactivo", jugador.getNombre());
        } else {
            if (jugador.getInicioSession().plusMinutes(jugador.getTiempoJuego()).isBefore(LocalDateTime.now())) {
                jugador.setActivo(false);
                LOGGER.warn("CasinoEngineService -> evaluateCanPlay() -> Jugador [{}] no tiene tiempo de juego, inicioSession: [{}], puede jugar hasta: [{}], tiempoactual: [{}], actualizando a inactivo", jugador.getNombre(), jugador.getInicioSession(), jugador.getInicioSession().plusMinutes(jugador.getTiempoJuego()), LocalDateTime.now());
            }
        }
        return jugadorRepository.save(jugador);
    }

    private boolean evaluateApuestaIsOk(Juego juego, BigDecimal apuesta){
        if(apuesta.compareTo(juego.getAp_min())>=0 && apuesta.compareTo(juego.getAp_max())<=0){
            return true;
        }else{
            LOGGER.warn("CasinoEngineService -> evaluateApuestaIsOk() -> apuesta realizada [{}] incorrecta. Datos apuestas para el juego [{}]: ap_min [{}], ap_max [{}] ", apuesta, juego.getNombre(), juego.getAp_min(), juego.getAp_max());
            return false;
        }
    }

    private Jugador updateBalance(Jugador jugador, BigDecimal importe, boolean win){
        if(win){
            LOGGER.info("CasinoEngineService -> updateBalance() -> Jugador [{}], balance actual [{}], se le suma al balance el Premio: [{}]", jugador.getNombre(), jugador.getBalance(), importe);
            jugador.setBalance(jugador.getBalance().add(importe));
        }else{
            LOGGER.info("CasinoEngineService -> updateBalance() -> Jugador [{}], balance actual [{}], se le retira del balance la apuesta: [{}]", jugador.getNombre(), jugador.getBalance(), importe);
            jugador.setBalance(jugador.getBalance().subtract(importe));
        }
        LOGGER.info("CasinoEngineService -> updateBalance() -> Jugador [{}], balance actualizado a [{}] ", jugador.getNombre(), jugador.getBalance());

        return jugadorRepository.save(jugador);
    }

}
