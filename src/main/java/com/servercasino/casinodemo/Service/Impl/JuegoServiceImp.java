package com.servercasino.casinodemo.Service.Impl;


import com.servercasino.casinodemo.Service.JuegoService;
import com.servercasino.casinodemo.constants.ErrorMessages;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.repository.JuegoRepository;
import com.servercasino.casinodemo.domain.Juego;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class JuegoServiceImp implements JuegoService {
    @Autowired
    JuegoRepository repository;

    @Override
    public Juego findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Juego> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Juego create(BigDecimal premio, float probabilidad, BigDecimal ap_max, BigDecimal ap_min, String nombre) {
        if (StringUtils.isEmpty(nombre))
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_NOMBRE_NULL);

        Juego juego = new Juego();
        juego.setNombre(nombre);
        juego.setPremio(premio);
        juego.setProbabilidad(probabilidad);
        juego.setAp_min(ap_min);
        juego.setAp_max(ap_max);

        return repository.save(juego);

    }
}
