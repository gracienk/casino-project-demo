package com.servercasino.casinodemo.Service.Impl;

import com.servercasino.casinodemo.Service.JugadaService;
import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.repository.JugadaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static com.servercasino.casinodemo.constants.ErrorMessages.ARGUMENT_EXCEPTION_NULL;


@Service
public class JugadaServiceImpl implements JugadaService {

    @Autowired
    private JugadaRepository repository;

    @Override
    public Jugada findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);
        }
        Optional<Jugada> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public List<Jugada> findByJuego(Juego juego) {

        if(juego == null){
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);
        }

        return repository.findByJuego(juego);
    }

    @Override
    public Jugada create(Jugador jugador, Juego juego, BigDecimal apuesta) {

        if (jugador == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);
        if (juego == null)
            throw new IllegalArgumentException(ARGUMENT_EXCEPTION_NULL);

        Jugada jugada = new Jugada();
        jugada.setJugador(jugador);
        jugada.setJuego(juego);
        jugada.setApuesta(apuesta);
        jugada.setWin(false);

        return repository.save(jugada);
    }
}
