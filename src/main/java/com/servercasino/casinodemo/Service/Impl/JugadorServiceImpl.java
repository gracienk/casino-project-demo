package com.servercasino.casinodemo.Service.Impl;

import com.servercasino.casinodemo.constants.ErrorMessages;
import com.servercasino.casinodemo.domain.Proveedor;
import com.servercasino.casinodemo.repository.JugadorRepository;
import com.servercasino.casinodemo.Service.JugadorService;
import com.servercasino.casinodemo.domain.Jugador;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Service
public class JugadorServiceImpl implements JugadorService {
    private static final Logger LOGGER=LoggerFactory.getLogger(JugadorServiceImpl.class);
    @Autowired
    JugadorRepository repository;

    @Override
    public List<Jugador> findAll() {
        return repository.findAll();
    }

    @Override
    public Jugador findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Jugador> optional = repository.findById(id);
        return optional.orElse(null);
    }

    @Override
    public Jugador create(BigDecimal balance, int tiempoJuego, boolean activo, String nombre, Proveedor proveedor) {

        if (tiempoJuego<0)
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_MENOR_0);
        if (StringUtils.isEmpty(nombre))
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_NOMBRE_NULL);

        Jugador jugador = new Jugador();
        jugador.setBalance(balance);
        jugador.setInicioSession(LocalDateTime.now());
        jugador.setTiempoJuego(tiempoJuego);
        jugador.setActivo(activo);
        jugador.setNombre(nombre);
        jugador.setProveedor(proveedor);
        LOGGER.info("Create jugador [{}], del proveedor [{}]", nombre, proveedor.getNombre());
        return repository.save(jugador);
    }
}
