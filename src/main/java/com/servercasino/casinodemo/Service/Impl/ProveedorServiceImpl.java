package com.servercasino.casinodemo.Service.Impl;

import com.servercasino.casinodemo.Service.ProveedorService;
import com.servercasino.casinodemo.constants.ErrorMessages;
import com.servercasino.casinodemo.domain.Proveedor;
import com.servercasino.casinodemo.repository.ProveedorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProveedorServiceImpl implements ProveedorService {
    @Autowired
    private ProveedorRepository repository;

    @Override
    public Proveedor findById(String id) {
        if(id == null){
            throw new IllegalArgumentException(ErrorMessages.ARGUMENT_EXCEPTION_ID_NULL);
        }
        Optional<Proveedor> optional = repository.findById(id);
        return optional.orElse(null);
    }
}
