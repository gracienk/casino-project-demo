package com.servercasino.casinodemo.Service.Impl;

import com.servercasino.casinodemo.Service.TransaccionService;
import com.servercasino.casinodemo.constants.ErrorMessages;
import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Transaccion;
import com.servercasino.casinodemo.repository.TransaccionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class TransaccionServiceImpl implements TransaccionService {

    @Autowired
    TransaccionRepository repository;

    @Override
    public List<Transaccion> findAll() {
        return repository.findAll();
    }

    @Override
    public List<Transaccion> findByJugada(Jugada jugada) { return repository.findByJugada(jugada); }

}
