package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Juego;
import java.math.BigDecimal;

public interface JuegoService {

    Juego findById(String id);
    Juego create(BigDecimal premio, float probabilidad, BigDecimal ap_max, BigDecimal ap_min, String nombre);

}
