package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Jugador;

import java.math.BigDecimal;
import java.util.List;

public interface JugadaService {
    Jugada findById(String id);
    List<Jugada> findByJuego(Juego juego);
    Jugada create(Jugador jugador, Juego juego, BigDecimal apuesta);
}
