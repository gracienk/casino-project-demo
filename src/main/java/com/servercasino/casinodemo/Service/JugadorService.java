package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Proveedor;
import com.servercasino.casinodemo.domain.Jugador;

import java.math.BigDecimal;
import java.util.List;

public interface JugadorService {

    List<Jugador> findAll();
    Jugador findById(String Id);
    Jugador create(BigDecimal balance, int tiempoJuego, boolean activo, String nombre, Proveedor proveedor);

}
