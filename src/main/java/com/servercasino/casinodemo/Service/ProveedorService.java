package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Proveedor;

public interface ProveedorService {
    Proveedor findById(String id);
}
