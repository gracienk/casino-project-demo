package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Transaccion;

import java.math.BigDecimal;
import java.util.List;

public interface TransaccionService  {
    List<Transaccion> findAll();
    List<Transaccion> findByJugada(Jugada jugada);
}
