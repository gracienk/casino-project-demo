package com.servercasino.casinodemo.api;

import com.servercasino.casinodemo.Service.JugadorService;
import com.servercasino.casinodemo.Service.ProveedorService;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Proveedor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.List;

import static com.servercasino.casinodemo.constants.RequestApi.*;

@RestController
@RequestMapping(API_REQUEST_MAPPING+JUGADOR_REQUEST_MAPPING)
public class JugadorController {

    @Autowired
    private JugadorService jugadorService;
    @Autowired
    private ProveedorService proveedorService;

    @GetMapping
    @ResponseStatus(value = HttpStatus.OK)
    public List<Jugador> findAll() {
        return jugadorService.findAll();
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    public Jugador create(@RequestParam("balance") BigDecimal balance,
                          @RequestParam("tiempoJuego") int tiempoJuego,
                          @RequestParam("activo") boolean activo,
                          @RequestParam("nombre") String nombre,
                          @RequestParam("id_proveedor") String id_proveedor
                          ) {

        Proveedor proveedor = proveedorService.findById(id_proveedor);
        return jugadorService.create(balance, tiempoJuego, activo, nombre, proveedor);

    }

}
