package com.servercasino.casinodemo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.VendorExtension;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket getDocket() {
        String version = "1.0.0";
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(version)
                .select()
                // Indicamos los controladors que queremos publicar en el API
                .apis(RequestHandlerSelectors.basePackage("com.servercasino.casinodemo.api"))
                .build()
                // Indicamos que no muestre los mensajes de respuesta por defecto
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo(version));
    }

    private ApiInfo apiInfo(String version) {
        List<VendorExtension> emptyList = Collections.emptyList();
        String termsOfServiceUrl = null;
        String license = null;
        String licenseUrl = null;
        return new ApiInfo(
                "Rest API",
                "Rest API to access to Game services.",
                version,
                termsOfServiceUrl,
                new Contact("Casino Server Test", "http://davidcasino.cat",
                        "admin@davidcasino.cat"),
                license, licenseUrl, emptyList);
    }

}
