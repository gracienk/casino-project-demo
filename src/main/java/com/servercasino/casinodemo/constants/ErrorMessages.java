package com.servercasino.casinodemo.constants;

public class ErrorMessages {

    private ErrorMessages() {
        throw new IllegalStateException("Utility class");
    }

    public static final String ARGUMENT_EXCEPTION_ID_NULL = "Id cannot be null";
    public static final String ARGUMENT_EXCEPTION_NOMBRE_NULL = "Nombre cannot be empty";
    public static final String ARGUMENT_EXCEPTION_NULL = "Argument cannot be null";
    public static final String ARGUMENT_EXCEPTION_JUGADA_NULL = "Argument JUGADA cannot be null";
    public static final String JUGADOR_EXCEPTION = "Jugador inactivo, no se puede crear jugada";
    public static final String ARGUMENT_EXCEPTION_MENOR_0 = "Argument cannot be menor a 0";


}
