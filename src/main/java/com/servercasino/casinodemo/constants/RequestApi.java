package com.servercasino.casinodemo.constants;

public class RequestApi {

    private RequestApi() {
        throw new IllegalStateException("Utility class");
    }

    public static final String API_REQUEST_MAPPING = "/api";
    public static final String JUGADOR_REQUEST_MAPPING = "/jugador";
    public static final String ID_REQUEST_MAPPING = "/{id}";
}
