package com.servercasino.casinodemo.domain;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
public class GenericEntity implements Serializable {
    @Id
    @Column(name = "ID")
    private String id;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    @PrePersist
    public void handlePrePersist(){
        // Assignem un id únic
        this.id = UUID.randomUUID().toString();
    }
}

