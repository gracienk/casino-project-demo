package com.servercasino.casinodemo.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="JUEGO")
public class Juego extends GenericEntity{

    @Column(name="PREMIO")
    private BigDecimal premio;

    @Column(name="PROBABILIDAD")
    private float probabilidad;

    @Column(name="AP_MAX")
    private BigDecimal ap_max;

    @Column(name="AP_MIN")
    private BigDecimal ap_min;

    @Column(name="NOMBRE")
    private String nombre;

    public BigDecimal getPremio() {
        return premio;
    }

    public void setPremio(BigDecimal premio) {
        this.premio = premio;
    }

    public float getProbabilidad() {
        return probabilidad;
    }

    public void setProbabilidad(float probabilidad) {
        this.probabilidad = probabilidad;
    }

    public BigDecimal getAp_max() { return ap_max; }

    public void setAp_max(BigDecimal ap_max) { this.ap_max = ap_max; }

    public BigDecimal getAp_min() { return ap_min; }

    public void setAp_min(BigDecimal ap_min) { this.ap_min = ap_min; }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
