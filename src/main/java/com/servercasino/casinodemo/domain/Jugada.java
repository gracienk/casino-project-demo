package com.servercasino.casinodemo.domain;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="JUGADA")
public class Jugada extends GenericEntity{

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_JUGADOR")
    private Jugador jugador;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_JUEGO")
    private Juego juego;

    @Column(name="WIN")
    private boolean win;

    @Column(name="APUESTA")
    private BigDecimal apuesta;

    public BigDecimal getApuesta() { return apuesta; }

    public void setApuesta(BigDecimal apuesta) { this.apuesta = apuesta; }

    public Jugador getJugador() { return jugador; }

    public void setJugador(Jugador jugador) {
        this.jugador = jugador;
    }

    public Juego getJuego() {
        return juego;
    }

    public void setJuego(Juego juego) {
        this.juego = juego;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }
}
