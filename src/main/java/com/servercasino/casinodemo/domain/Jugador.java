package com.servercasino.casinodemo.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;


@Entity
@Table(name = "JUGADOR")
public class Jugador extends GenericEntity{

    @Column(name = "BALANCE")
    private BigDecimal balance;

    @Column(name = "INICIO_SESSION")
    private LocalDateTime inicioSession;

    @Column(name = "TIEMPO_JUEGO")
    private int tiempoJuego;

    @Column(name = "ACTIVO")
    private boolean activo;

    @Column(name = "NOMBRE")
    private String nombre;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_PROVEEDOR")
    private Proveedor proveedor;

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public LocalDateTime getInicioSession() {
        return inicioSession;
    }

    public void setInicioSession(LocalDateTime inicioSession) {
        this.inicioSession = inicioSession;
    }

    public int getTiempoJuego() {
        return tiempoJuego;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    public void setTiempoJuego(int tiempoJuego) {
        this.tiempoJuego = tiempoJuego;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }
}
