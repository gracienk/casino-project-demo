package com.servercasino.casinodemo.domain;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="PROVEEDOR")
public class Proveedor extends GenericEntity{

    private String nombre;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
}
