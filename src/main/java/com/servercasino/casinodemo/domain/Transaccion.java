package com.servercasino.casinodemo.domain;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

@Entity
@Table(name="TRANSACCION")
public class Transaccion extends GenericEntity{

    @Column(name="IMPORTE")
    private BigDecimal importe;

    @Column(name="CREATION_DATE")
    private LocalDateTime creation_date;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ID_JUGADA")
    private Jugada jugada;

    public BigDecimal getImporte() {
        return importe;
    }

    public void setImporte(BigDecimal importe) {
        this.importe = importe;
    }

    public LocalDateTime getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }

    public Jugada getJugada() { return jugada; }

    public void setJugada(Jugada jugada) { this.jugada = jugada; }
}
