package com.servercasino.casinodemo.repository;

import com.servercasino.casinodemo.domain.Juego;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JuegoRepository extends JpaRepository<Juego, String> {
    List<Juego> findAll();
    Juego findByNombre(String nombre);
}
