package com.servercasino.casinodemo.repository;

import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugada;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface JugadaRepository extends JpaRepository<Jugada, String> {
    List<Jugada> findByJuego(Juego juego);
}
