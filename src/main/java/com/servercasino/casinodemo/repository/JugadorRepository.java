package com.servercasino.casinodemo.repository;

import com.servercasino.casinodemo.domain.Proveedor;
import com.servercasino.casinodemo.domain.Jugador;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JugadorRepository extends JpaRepository<Jugador, String>{
    List<Jugador> findAll();
    List<Jugador> findByProveedor(Proveedor proveedor);
}