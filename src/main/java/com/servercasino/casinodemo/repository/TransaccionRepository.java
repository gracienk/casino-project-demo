package com.servercasino.casinodemo.repository;

import com.servercasino.casinodemo.domain.Jugada;
import com.servercasino.casinodemo.domain.Transaccion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TransaccionRepository extends JpaRepository<Transaccion, String> {
    List<Transaccion> findByJugada(Jugada jugada);
}
