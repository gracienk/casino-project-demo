package com.servercasino.casinodemo;

import com.servercasino.casinodemo.Service.CasinoEngineService;
import com.servercasino.casinodemo.Service.JuegoService;
import com.servercasino.casinodemo.Service.JugadorService;
import com.servercasino.casinodemo.Service.ProveedorService;
import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Proveedor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApuestaLimitesTest {

    @Autowired
    CasinoEngineService casinoEngineService;
    @Autowired
    JugadorService jugadorService;
    @Autowired
    JuegoService juegoService;
    @Autowired
    ProveedorService proveedorService;

    @Test
    public void limitesApuestaTest (){
        //test jugada apuesta fuera límites(min) Mus MAX_AP 20  MIN_AP 10
        Juego mus = juegoService.findById("2");
        Proveedor proveedorTest = proveedorService.findById("1");

        Jugador jugadorMus = jugadorService.create(new BigDecimal(100.50), 30, true, "PlayerCanPlay", proveedorTest );
        casinoEngineService.realizarJugada(jugadorMus, mus, new BigDecimal(8));
        casinoEngineService.realizarJugada(jugadorMus, mus, new BigDecimal(25));
    }

}
