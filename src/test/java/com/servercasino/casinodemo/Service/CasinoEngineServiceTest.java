package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.*;
import com.servercasino.casinodemo.repository.JugadorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CasinoEngineServiceTest {
    @Autowired
    CasinoEngineService casinoEngineService;
    @Autowired
    JugadorService jugadorService;
    @Autowired
    JuegoService juegoService;
    @Autowired
    ProveedorService proveedorService;
    @Autowired
    TransaccionService transaccionService;

    @Test
    public void AllMethods(){

        Juego blackjack = juegoService.findById("1");
        Juego mus = juegoService.findById("2");
        Proveedor proveedorTest = proveedorService.findById("1");

        //test jugadas hasta agotar el saldo
        Jugador jugadorBlack = jugadorService.create(new BigDecimal(100.0), 30, true, "jugadorInBlackJack", proveedorTest);
        Jugada testJugadaBlack = null;

        Jugador jugadorMus = jugadorService.create(new BigDecimal(100.0), 30, true, "jugadorInMUS", proveedorTest);
        Jugada testJugadaMUS = null;

         do {
             jugadorBlack = jugadorService.findById(jugadorBlack.getId());
             testJugadaBlack = casinoEngineService.realizarJugada(jugadorBlack, blackjack, this.randomApuesta(50));

             jugadorMus = jugadorService.findById(jugadorMus.getId());
             testJugadaMUS = casinoEngineService.realizarJugada(jugadorMus, mus, this.randomApuesta(50));
        } while (jugadorBlack.isActivo() || jugadorMus.isActivo() );

        List<Transaccion> blackJackTransaccionListByJugada = transaccionService.findByJugada(testJugadaBlack);
        assertNotNull(blackJackTransaccionListByJugada);
        List<Transaccion> musTransaccionListByJugada = transaccionService.findByJugada(testJugadaMUS);
        assertNotNull(musTransaccionListByJugada);
    }

    private BigDecimal randomApuesta(int range) {
        BigDecimal max = new BigDecimal(range);
        BigDecimal randFromDouble = new BigDecimal(Math.random());
        BigDecimal actualRandomDec = randFromDouble.multiply(max);
        actualRandomDec = actualRandomDec
                .setScale(2, BigDecimal.ROUND_DOWN);
        return actualRandomDec;
    }
}
