package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Proveedor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JuegoServiceTest {

    @Autowired
    JuegoService juegoService;
    @Autowired
    JugadorService jugadorService;
    @Autowired
    ProveedorService proveedorService;

    @Test
    public void AllMethods(){
        //creados al arrangar app en el data.sql
        Juego juego = juegoService.findById("1");
        assertNotNull(juego);
        Proveedor proveedor = proveedorService.findById("1");
        assertNotNull(proveedor);

        Juego juegoCreate = juegoService.create(new BigDecimal("2000.25"), 4.5f, new BigDecimal("10.00"),new BigDecimal("5.00"), "testjack");
        assertNotNull(juegoCreate);
        Jugador jugadorCreate = jugadorService.create(new BigDecimal("50.00"), 30, true, "playerOne", proveedor);
        assertNotNull(jugadorCreate);
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByIdFails() {
        juegoService.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createNameNullFails() {
        juegoService.create(new BigDecimal("2000.25"), 4.5f, new BigDecimal("5.5"),new BigDecimal("20"), null);
    }
}
