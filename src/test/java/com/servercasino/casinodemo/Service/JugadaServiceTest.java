package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.*;
import com.servercasino.casinodemo.repository.JugadorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JugadaServiceTest {

    @Autowired
    JugadaService jugadaService;
    @Autowired
    JugadorService jugadorService;
    @Autowired
    JuegoService juegoService;
    @Autowired
    ProveedorService proveedorService;

    @Test
    public void AllMethods() {
        //test Variables
        BigDecimal importe = new BigDecimal(50);
        BigDecimal balance = new BigDecimal(500);
        BigDecimal premio = new BigDecimal(250);
        BigDecimal ap_max = new BigDecimal(20);
        BigDecimal ap_min = new BigDecimal(5);

        Proveedor proveedorTest = proveedorService.findById("1");
        Jugador jugadorTest = jugadorService.create(balance, 30, true, "jugadorTest", proveedorTest);
        assertNotNull(jugadorTest);
        Juego juegoTestCreate = juegoService.create(premio, 5f,ap_max, ap_min, "juegoTest");
        assertNotNull(juegoTestCreate);
        Jugada jugada = jugadaService.create(jugadorTest, juegoTestCreate, importe);
        assertNotNull(jugada);


        Juego juegoTestRead = juegoService.findById("1");
        Jugada jugadaTest = jugadaService.create(jugadorTest, juegoTestRead, importe);
        List<Jugada> jugadaList = jugadaService.findByJuego(juegoTestRead);
        assertFalse(jugadaList.isEmpty());
        assertEquals("1", jugadaList.get(0).getId());
    }

    @Test(expected = IllegalArgumentException.class)
    public void findByIdFails() {
        jugadaService.findById(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createFailsdByJugador() {
        Proveedor proveedorTest = proveedorService.findById("1");
        Juego juegoTestCreate = juegoService.create(new BigDecimal(1500), 5f,new BigDecimal(10), new BigDecimal(40), "juegoTest");
        Jugada jugada = jugadaService.create(null, juegoTestCreate, new BigDecimal(10));
    }
    @Test(expected = IllegalArgumentException.class)
    public void createFailsdByJuego() {
        Proveedor proveedorTest = proveedorService.findById("1");
        Jugador jugadorTest = jugadorService.create(new BigDecimal(500), 30, true, "testJugador", proveedorTest);
        Jugada jugada = jugadaService.create(jugadorTest, null, new BigDecimal(10));
    }
    @Test(expected = IllegalArgumentException.class)
    public void listJugadaByJuegoFailsdByJuego() {
        List<Jugada> listJugadaByJuego = jugadaService.findByJuego(null);
    }
}
