package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Jugador;
import com.servercasino.casinodemo.domain.Proveedor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class JugadorServiceTest {

    @Autowired
    private JugadorService service;
    @Autowired
    private ProveedorService proveedorService;

    @Test
    public void AllMethods(){
        Jugador jugador = service.findById("1");
        assertNotNull(jugador);

        //create Method
        Proveedor proveedorTest = proveedorService.findById("2");
        Jugador jugadorCreate = service.create( new BigDecimal("50"), 30, true, "playerOne", proveedorTest);
        assertNotNull(jugadorCreate);

        List<Jugador> jugadorList = service.findAll();
        assertFalse(jugadorList.isEmpty());
    }
}

