package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.Proveedor;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProveedorTest {
    @Autowired
    ProveedorService proveedorService;

    @Test
    public void AllMethods(){
        Proveedor proveedor =  proveedorService.findById("1");
        assertNotNull(proveedor);
    }
}
