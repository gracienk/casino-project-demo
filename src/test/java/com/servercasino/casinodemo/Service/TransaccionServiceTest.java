package com.servercasino.casinodemo.Service;

import com.servercasino.casinodemo.domain.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransaccionServiceTest {

    @Autowired
    TransaccionService transaccionService;
    @Autowired
    JugadaService jugadaService;

    @Test
    public void AllMethods(){
        Jugada jugada = jugadaService.findById("1");

        List<Transaccion> transaccionList = transaccionService.findAll();
        assertFalse(transaccionList.isEmpty());
        assertFalse(StringUtils.isEmpty(transaccionList.get(0).getId()));

        List<Transaccion> transaccionListJugada = transaccionService.findByJugada(jugada);
        assertFalse(transaccionListJugada.isEmpty());
        assertFalse(StringUtils.isEmpty(transaccionListJugada.get(0).getId()));
    }

}
