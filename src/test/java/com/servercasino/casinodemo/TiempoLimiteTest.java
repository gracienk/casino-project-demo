package com.servercasino.casinodemo;

import com.servercasino.casinodemo.Service.CasinoEngineService;
import com.servercasino.casinodemo.Service.JuegoService;
import com.servercasino.casinodemo.Service.JugadorService;
import com.servercasino.casinodemo.domain.Juego;
import com.servercasino.casinodemo.domain.Jugador;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TiempoLimiteTest {

    @Autowired
    CasinoEngineService casinoEngineService;
    @Autowired
    JugadorService jugadorService;
    @Autowired
    JuegoService juegoService;

    @Test
    public void limitesApuestaTest () {
        //test jugada jugador sin tiempo juego
        Juego blackjack = juegoService.findById("1");
        Jugador endTimeJugador = jugadorService.findById("2");
        casinoEngineService.realizarJugada(endTimeJugador, blackjack, new BigDecimal(8));
        casinoEngineService.realizarJugada(endTimeJugador, blackjack, new BigDecimal(10));
    }
}
